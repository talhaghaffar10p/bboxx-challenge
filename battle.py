class Battle:
    # defines results in diffrent cases in a battle
    
    @staticmethod
    def attack(attacker, defender):
        attack_points = attacker.base_attack + 0.5
        defend_points = defender.base_defence

        if attacker.equipped:
            attack_points += attacker.equipped.attack

        if defender.equipped:
            defend_points += defender.equipped.defence

        return (
            (attacker, defender)
            if attack_points > defend_points
            else (defender, attacker)
        )

    @staticmethod
    def kill_knight(knight, status=1):
       
        prize = knight.equipped
        last_pos = knight.position

        knight.update_status(status)
        knight.position = None
        knight.equipped = None
        knight.base_attack = 0
        knight.base_defence = 0

        return prize, last_pos
