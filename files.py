from pathlib import Path
from json import dumps


class Files:
    @staticmethod
    def read_moves():
        # Read moves from moves.txt file having a specific format
        moves_set = []
        content = Path('./moves.txt').read_text()
        moves = content.strip().split('\n')
        if moves[0] == 'GAME-START':
            moves.pop(0)
        if moves[-1] == 'GAME-END':
            moves.pop()
        
        for m in moves:
            moves_set.append(list(m.split(':')))
        print('\n')
        print('--------------------------------------------------------------------------------------')
        print("Moves for the game are: \n" + str(moves_set) + '\n')
        

        return moves_set

    @staticmethod
    def gamestate(knights: list, items: list):
        result = {}

        for k in knights:
            k_result = (k.position.to_json() if k.position else None, k.status)
            if k.equipped:
                k_result += (
                    k.equipped.name,
                    k.base_attack + k.equipped.attack,
                    k.base_defence + k.equipped.defence,
                )
            else:
                k_result += (None, k.base_attack, k.base_defence)
            result[k.colour] = k_result

        for i in items:
            i_result = (i.position.to_json(), i.position.knight is not None)
            result[i.name] = i_result

        return result

    @staticmethod
    def write_to_json(state):
        return Path('./final_state.json').write_text(dumps(state))
