from dataclasses import dataclass, field
from battle import Battle
from position import Position

class Arena:
    # Movements on board

    def __init__(self):
        board = []
        for y in range(0, 8):
            row = [Position(y, x) for x in range(0, 8)]
            board.append(row)
        self.board = board

    def move_knight(self, knight, direction):
        knight.position.knight = None
        try:
            temp_position = self.position_direction(direction, knight.position)
        except Drowned:
            prize, last_pos = Battle.kill_knight(knight, status=2)
            
            if self.drop_prize(prize, last_pos):
                pass
                
        else:
            if self.knight_in_box(temp_position):
                winner, loser = Battle.attack(knight, temp_position.knight)
                self.knight_movement(winner, temp_position)
                prize, last_pos = Battle.kill_knight(loser)
            
                if self.drop_prize(prize, last_pos):
                    pass
            
                return winner

            if self.empty_box(temp_position):
                self.knight_movement(knight, temp_position)
    
            elif self.item_in_box(temp_position):
                self.knight_movement(knight, temp_position)
                temp_position.items = sorted(temp_position.items, key = lambda a: a.priority)
                if not knight.equipped:
                    knight.equipped = temp_position.items.pop()

            return knight

    def drop_prize(self, item, position):  
        if item:
            item.position = position
            position.items.append(item)
            position.items = sorted(position.items, key = lambda a: a.priority)
            return True

    def knight_movement(self, knight, position):
        knight.position = position
        position.knight = knight
        if knight.equipped:
            knight.equipped.position = position
    
    def position_direction(self, direction: str, old_pos: Position):
        
        dir_map = {
            'N': (old_pos.y - 1, old_pos.x),
            'S': (old_pos.y + 1, old_pos.x),
            'E': (old_pos.y, old_pos.x + 1),
            'W': (old_pos.y, old_pos.x - 1),
        }
        y, x = dir_map[direction]

        if x < 0 or x > 7 or y < 0 or y > 7:
            raise Drowned('Knight drowned')

        return self.board[y][x]

    def empty_box(self, position):
        return not position.knight and len(position.items) is 0

    def item_in_box(self, position):
        return len(position.items) > 0

    def knight_in_box(self, position):
        return position.knight is not None

class Drowned(Exception):
    pass
