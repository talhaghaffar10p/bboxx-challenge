# Bboxx Coding Challenge - BATTLING KNIGHTS
> Python
> A coding challenge by Bboxx, which is a chess like decision based games. According to the rules the Knights will move, attack, defencd and depending on the moves the final state of all the knights and items will be presented as output in a json file.


## Requirements
Python 3.7


## Usage

Linux:

1) Run Application
```
python run.py OR python3 run.py
```


## Structure
Battling Knights consists of following classes,  
**• Knights: ** contain detail about the night like its id color position etc.  
**• Items: ** contain details such as name, priority and powers regarding the item (axe, Helmet, etc.)  
**• Arena: ** contain details regarding the board  
**• Files: ** deals with file system, reading writing to the files  
**• Position: ** deals with position the items and knights within the board   
**• Run: ** the main class which Run the game and compile everything together  

## Instructions
moves.txt needs to be provided with set of moves having specific format (defined in chhallenge).  
final output will be in json format in a file `final_state.json`.

## Developer 
**Muhammad Talha Ghaffar [talha.ghaffar@tenpearls.com]**
