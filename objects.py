import json
from dataclasses import dataclass, field
from position import Position


states = ['LIVE', 'DEAD', 'DROWNED']

@dataclass
class Item:
    # Items (axe, helmet, etc)
    name: str
    priority: int
    position: Position
    attack: int
    defence: int

@dataclass
class Knight:
    # knights details
    id: str
    colour: str
    position: Position
    status: str = states[0]
    equipped: Item = None
    base_attack: int = 1
    base_defence: int = 1

    def update_status(self, idx):
        self.status = states[idx]
