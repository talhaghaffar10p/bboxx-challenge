from arena import Arena
from files import Files
from objects import Item
from objects import Knight


class Run:
    def setup_board(self):
        # Starting Point, setting up the board
        self.arena = Arena()
        board = self.arena.board

        self.R = Knight('R', 'red', board[0][0])
        self.Y = Knight('Y', 'yellow', board[0][7])
        self.B = Knight('B', 'blue', board[7][0])
        self.G = Knight('G', 'green', board[7][7])

        board[0][0].knight = self.R
        board[0][7].knight = self.Y
        board[7][0].knight = self.B
        board[7][7].knight = self.G

        self.item_axe = Item('Axe', 4, board[2][2], 2, 0)
        self.item_dagger = Item('Dagger', 2, board[2][5], 1, 0)
        self.item_magicstaff = Item('MagicStaff', 3, board[5][2], 1, 1)
        self.item_helmet = Item('Helmet', 1, board[5][5], 0, 1)

        board[2][2].items.append(self.item_axe)
        board[2][5].items.append(self.item_dagger)
        board[5][2].items.append(self.item_magicstaff)
        board[5][5].items.append(self.item_helmet)

        return (
            self.arena,
            self.R,
            self.Y,
            self.B,
            self.G,
            self.item_axe,
            self.item_dagger,
            self.item_magicstaff,
            self.item_helmet,
        )

    def exec_moves(self):
        movements = Files.read_moves()

        for (knight_id, direction) in movements:
            knight = getattr(self, knight_id)
            self.arena.move_knight(knight, direction)

# main
if __name__ == '__main__':
    game = Run()

    (
        arena,
        knight_R,
        knight_Y,
        knight_B,
        kinght_G,
        item_axe,
        item_dagger,
        item_magicstaff,
        item_helmet,
    ) = game.setup_board()

    game.exec_moves()

    knights = [kinght_G,knight_Y,knight_B,knight_R]
    items = [item_axe, item_dagger, item_magicstaff, item_helmet]
    state = Files.gamestate(knights, items)
    Files.write_to_json(state)
    print('--------------------------------------------------------------------------------------')
    print('Final state of knights and items is: \n' + str(state))
    print('\n')
    print('--------------------------------------------------------------------------------------')

